
#   -----------------------------
#   0. DEFINE COLORS
#   -----------------------------

PURPLE='\e[0;35m'
DARKGRAY='\e[1;30m'
LIGHTRED='\e[1;31m'
LIGHTBLUE='\e[1;34m'
LIGHTPURPLE='\e[1;35m'
NC='\e[m'

# Import source for github
export GITAWAREPROMPT=~/.bash/git-aware-prompt
source "${GITAWAREPROMPT}/main.sh"

#   ----------------------------
#   1. PROMPT
#   ----------------------------

# User
export PS1="\n$PURPLE\u$NC@$LIGHTPURPLE\h$NC /$PURPLE\W$NC \[$txtcyn\]\$git_branch\[$txtred\]\$git_dirty\[$txtrst\]\$ "


#   -----------------------------
#   2.  MAKE TERMINAL BETTER
#   -----------------------------

alias cp='cp -iv'                           # Preferred 'cp' implementation
alias mv='mv -iv'                           # Preferred 'mv' implementation
alias mkdir='mkdir -pv'                     # Preferred 'mkdir' implementation
alias ll='ls -FGlAhp'                       # Preferred 'ls' implementation
alias less='less -FSRXc'                    # Preferred 'less' implementation
cd() { builtin cd "$@"; ll; }               # Always list directory contents upon 
'cd'
alias cd..='cd ../'                         # Go back 1 directory level (for fast 
typers)
alias ..='cd ../'                           # Go back 1 directory level
alias ...='cd ../../'                       # Go back 2 directory levels
alias .3='cd ../../../'                     # Go back 3 directory levels
alias .4='cd ../../../../'                  # Go back 4 directory levels
alias .5='cd ../../../../../'               # Go back 5 directory levels
alias .6='cd ../../../../../../'            # Go back 6 directory levels

alias ~="cd ~"                              # ~:            Go Home
alias c='clear'                             # c:            Clear terminal display
alias which='type -all'                     # which:        Find executables
alias path='echo -e ${PATH//:/\\n}'         # path:         Echo all
