Bash Config
===========

This repository contains all personal configurations.
Last Update : Tue 8 Dec 2015

<pre>
/repo
|
|-- bash-profile
|   |-- /osx
|   |   |-- /.bash (from github repo)
|   |   |-- .bash_profile
|   |
|   |-- /linux
|       |-- /.bash (from github repo)
|       |-- .bash_profile
|
|-- sublime-text-3
    |-- /Installed Packages
    |-- /Packages
    |-- sublime.backup
</pre>
